import express from 'express'
import fileUpload from 'express-fileupload'
import { uploadFile, getFiles, getFile, downloadFile, getFileURL } from './s3.js'

const app = express()

// app.use(fileUpload({
//     useTempFiles: true,
//     tempFileDir: './files'
// }))

app.get('/files', async (req, res) => {
    const result = await getFiles(req.body.filename)
    res.json(result)
})

app.get('/files/:fileName', async (req, res) => {
    const result = await getFileURL(req.params.fileName)
    res.json({
        url: result
    })
})

app.get('/downloadfile/:fileName', async (req, res) => {
    await downloadFile(req.params.fileName)
    res.json({message: "archivo descargado"})
})


app.post('/files', async (req, res) => {
    const result = await uploadFile()
    res.json({ result })
})

app.use(express.static('images'))

app.listen(4000)
console.log(`Server on port ${4000}`)